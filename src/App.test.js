import React from 'react';
import App from './App';
//import * as testes from './1__tests__'
import { getAllPokemonsQuery } from './store/queries'
//import nock from 'n';

import {
  render,
  fireEvent,
  cleanup,
  waitForElement,
  act,
  waitFor,
  wait,
  waitForDomChange,
  waitForElementToBeRemoved
} from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
jest.setTimeout(30000);

const variables = {first:151}

jest.mock('./store/queries')
it('Render app', async () => {
  beforeAll(() => {
    jest.mock('https://graphql-pokemon.now.sh/')
      .post(JSON.stringify({ getAllPokemonsQuery, variables }))
      .reply(200, pokemons);
  })

  const { getByTestId, getAllByTestId, getByText, getAllByText } = render(<App />);
  expect(getByText('Loading..')).toBeInTheDocument()
  expect(await getByTestId("searchInput")).toBeTruthy()

  const card = await  waitForElement( () => getAllByTestId("card"))

  card.forEach((card)=> {
    expect(card).toBeVisible()
  })
})

it('Search Bulbasaur', async() => {
    beforeAll(() => {
      jest.mock('https://graphql-pokemon.now.sh/')
        .post(JSON.stringify({ getAllPokemonsQuery, variables }))
        .reply(200, pokemons);
    })
    const { getByTestId, getAllByTestId, getByText, getAllByText } = render(<App />);
    //procura input search
    const  search  = await getByTestId("searchInput")
    expect(search).toBeTruthy()
    //espera carregar os pokemons
    var cards = await  waitForElement( () => getAllByTestId("card"))
    expect(cards.length > 0)
    //digita algo no campo de pesquisa
    fireEvent.change(search, { target: { search: 'Bulbasaur' } })
    expect(search.search).toBe('Bulbasaur')
    cards = await  waitForElement( () => getAllByText("Bulbasaur"))
    expect(cards).toHaveLength(1)
  });

  // it('Detalhar Bulbasaur', async() => {
  //   beforeAll(() => {
  //     jest.mock('https://graphql-pokemon.now.sh/')
  //       .post(JSON.stringify({ getAllPokemonsQuery, variables }))
  //       .reply(200, pokemons);
  //   })
  //   const { getByTestId, getAllByTestId, getByText, getAllByText,getByRole , history } = render(<App />);
  //   //procura input search
  //   const  search  = await getByTestId("searchInput")
  //   expect(search).toBeTruthy()
  //   //espera carregar os pokemons
  //   var cards = await  waitForElement( () => getAllByTestId("card"))
  //   expect(cards.length > 0)
  //   //digita algo no campo de pesquisa
  //   fireEvent.change(search, { target: { search: 'Bulbasaur' } })
  //   expect(search.search).toBe('Bulbasaur')
  //   cards = await  waitForElement( () => getAllByText("Bulbasaur"))
  //   expect(cards).toHaveLength(1)
  //   fireEvent.click(getAllByTestId('detailBtn')[0],{ button: 2 })
  //   // var detail = await
  //   // expect(jest.fn()).toHaveBeenCalledTimes(1)
  //   // expect(jest.fn()).toHaveBeenLastCalledWith(0, 0)
  //   console.log('hist ', history)
  //   var cards = await waitForElement( () => getByTestId("tableDetalhe"))
  //   // expect(await waitForElement(() => getByTestId('tableDetalhe')))
  //   // expect(getByRole('heading')).toHaveTextContent('404 Not Found')
  // });