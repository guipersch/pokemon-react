import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { detailPokemon }  from '../../store/actions'
import { useSelector, useDispatch } from 'react-redux'
import { Card, Image, Label, Button } from 'semantic-ui-react'
import { push } from 'connected-react-router'

const imgStyle = {
  maxHeight:150,
  height:150
}
const cardStyle = {
  height:350,
  maxHeight:350,
  width:250
}

const CardPokemon = ({pokemon}) => {
  //const pokemon  = useSelector(state => state.pokemon)
  const dispatch = useDispatch()

  function detail() {
    dispatch(detailPokemon(pokemon))
    dispatch(push('/detalhe'))
  }
  
  return (
    <div >
      <Card style={cardStyle} data-testid='card' >
          <div  wrapped>
            <Image style={imgStyle}  src={pokemon.image} size='small' ui={true}/>
          </div>
         
          <Card.Content>
              <Card.Header> {pokemon.name} </Card.Header>
              <Card.Meta>
              </Card.Meta>
              <Card.Description>
                <p> <b>ID</b>  : {pokemon.id}</p>
                <p> <b>Number</b> : {pokemon.number}</p>
                  {pokemon.types.map( (tipo) => (
                     <Label >
                     {tipo}
                   </Label>
                  ))}
                 
              </Card.Description>
          </Card.Content>
          <Card.Content extra>
            <Button attached='bottom' data-testid='detailBtn' content='Detalhar' onClick={detail}/>            
          </Card.Content>
      </Card>
    </div>
  )
}

// const mapStateToProps = (state ) => console.log('state:',state)
// ({
//   pokemon: state.pokemon,
// })

 //const mapDispatchToProps = dispatch => bindActionCreators(actions,dispatch)

 export default CardPokemon
//export default CardPokemon