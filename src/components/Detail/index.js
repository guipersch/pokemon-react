import React,{  useEffect }  from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Table, Image, Container, Label, Button } from 'semantic-ui-react'
import { useParams} from "react-router";
import { Redirect } from 'react-router-dom'
import { push } from 'connected-react-router'

const Detail = () => {
    
    const pokemon = useSelector(state => state.pokemons.pokemon)
    //const params = useParams()
    const dispatch = useDispatch()

    function editPokemon() {
        //dispatch(detailPokemon(pokemon))
        dispatch(push('/editar'))
      }

    const backHome = () => {
        dispatch(push('/'))
    }

    // useEffect(() => {
    //     console.log('use effect')
    //     if (!pokemon){
    //         dispatch(push('/'))
    //     }
    //     // dispatch(requestAllPokemons())
    //     // getAllPokemons(getAllPokemonsQuery,{first:151}).then(response => {
    //     //   dispatch({type: 'SUCCESS_SEARCH_POKEMONS',payload: {data : response.data.pokemons}})
    //     // })
        
    //   },[])


// const getAllPokemons = async (query, variables = {}) => {
//     const response = await fetch(BASE_URL_API, {
//       method: 'POST',
//       headers: { 'Content-Type': 'application/json' },
//       body: JSON.stringify({ query, variables })
//     });
  
//     return response.json();
//   }
    if (!pokemon.id) {
        console.log('sempokemon')
        return <Redirect from='/detalhe' to='/'/>
    } else {
        return (
            <Container text> 
                <Table data-testid='tableDetalhe' celled>
                    <Table.Body>
                        <Table.Row>
                            <Table.Cell textAlign='center'>
                                <Image src={pokemon.image} size='tiny' ui={false} wrapped />
                            </Table.Cell>  
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell>
                                <Label size='large' color='black' ribbon>ID:</Label>
                                {pokemon.id}
                            </Table.Cell>   
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell>
                                <Label size='large' color='black' ribbon>Number:</Label>
                                {pokemon.number}
                            </Table.Cell>  
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell>
                                <Label size='large' color='black' ribbon>Name:</Label>
                                {pokemon.name}
                            </Table.Cell>   
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell>
                                <Label size='large' color='black' ribbon>Weight:</Label>
                                {pokemon.weight.minimum} - {pokemon.weight.maximum}
                            </Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell>
                                <Label size='large' color='black' ribbon>Height:</Label>
                                {pokemon.height.minimum} - {pokemon.height.maximum}
                            </Table.Cell> 
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell>
                                <Label size='large' color='black' ribbon>Classification:</Label>
                                {pokemon.classification}
                            </Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell>
                                <Label size='large' color='black' ribbon>Types:</Label>
                                {/* {pokemon.types} */}
                                {pokemon.types.map( (weak) => (
                                    <Label size='medium' color='grey'>
                                        {weak}
                                    </Label>
                                ))}
                            </Table.Cell>  
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell>
                                <Label size='large' color='black' ribbon>Resistant:</Label>
                                {pokemon.resistant.map( (resistant) => (
                                    <Label size='medium' color='green'>
                                        {resistant}
                                    </Label>
                                ))}
                            </Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell>
                                <Label size='large' color='black' ribbon>Weaknesses:</Label>
                                {pokemon.weaknesses.map( (weak) => (
                                    <Label size='medium' color='red'>
                                        {weak}
                                    </Label>
                                ))}
                            </Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell>
                                <Label size='large' color='black' ribbon>Flee rate:</Label>
                                {pokemon.fleeRate}
                            </Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell>
                                <Label size='large' color='black' ribbon>Max CP:</Label>
                                {pokemon.maxCP}
                            </Table.Cell> 
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell>
                                <Label size='large' color='black' ribbon>Max HP:</Label>
                                {pokemon.maxHP}
                            </Table.Cell> 
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell textAlign='center'>
                                <Button content='Editar' onClick={editPokemon}/>
                                <Button content='Voltar' onClick={backHome}/>   
                            </Table.Cell> 
                        </Table.Row>
                    </Table.Body> 
                </Table>
            </Container>
        )
    }
}



// id
// number
// name
// weight{
//   minimum
//   maximum
// }
// height{
//   minimum
//   maximum
// }
// classification
// types
// resistant
// weaknesses
// fleeRate
// maxCP
// maxHP
// image
// }

// const mapStateToProps = (state ) => ({
//   pokemons: state.pokemons,
// })

// const mapDispatchToProps = dispatch => bindActionCreators(actions,dispatch)

// export default connect(mapStateToProps, mapDispatchToProps)(CardPokemon))
export default Detail