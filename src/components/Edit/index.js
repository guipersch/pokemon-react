import React, { useState } from 'react'
import { Form, Button, Container } from 'semantic-ui-react'
import { useForm } from "react-hook-form";
import { useSelector, useDispatch } from 'react-redux'
import { Redirect } from 'react-router-dom'
import { savePokemon }  from '../../store/actions'
import { push } from 'connected-react-router'


const Edit = () =>{
    const { register, handleSubmit, watch, errors } = useForm();
    const pokemon = useSelector(state => state.pokemons.pokemon)
    const dispatch = useDispatch()


    const onSubmit = (data) =>{
        console.log(data)
        data.image = pokemon.image
        dispatch(savePokemon(data))
        dispatch(push('/detalhe'))

    } ;


    if (!pokemon.id) {
        console.log('sempokemon')
        return <Redirect from='/editar' to='/'/>
    } else {
        return (
            <Container>
                <Form onSubmit={handleSubmit(onSubmit)}>
                    <Form.Group widths='equal'>
                        <Form.Field>
                            <label>ID</label>
                            <input disabled={true} name='id' ref={register({ required: true })} placeholder='ID' defaultValue={pokemon.id} />
                        </Form.Field>
                        <Form.Field>
                            <label>Number</label>
                            <input data-testid='numberInput' name='number' ref={register({ required: true })} placeholder='Number' defaultValue={pokemon.number} />
                        </Form.Field>
                    </Form.Group>
                    <Form.Group widths='equal'>
                        <Form.Field>
                            <label>Name</label>
                            <input data-testid='nameInput' name='name' ref={register({ required: true })} placeholder='Name' defaultValue={pokemon.name} />
                        </Form.Field>
                        <Form.Field>
                            <label>Classification</label>
                            <input data-testid='classificationInput' name='classification' ref={register({ required: true })} placeholder='Classification' defaultValue={pokemon.classification} />
                            </Form.Field>
                    </Form.Group>   
                    <Form.Group widths='equal'>
                        <Form.Field>
                            <label>Min. weight</label>
                            <input data-testid='minWInput' name='weight.minimum' ref={register({ required: true, min: 0.1})} placeholder='Min'  defaultValue={pokemon.weight.minimum} />
                        </Form.Field>
                        <Form.Field>
                            <label>Max. weight</label>
                            <input data-testid='maxWInput' name='weight.maximum' ref={register({ required: true,min: 0.1 })} placeholder='Max'  defaultValue={pokemon.weight.maximum} />
                        </Form.Field>
                    </Form.Group>
                    <Form.Group widths='equal'>
                        <Form.Field>
                            <label>Min. height</label>
                            <input data-testid='minHInput' name='height.minimum' ref={register({ required: true })} placeholder='Min'  defaultValue={pokemon.height.minimum} />
                        </Form.Field>
                        <Form.Field>
                            <label>Max. height</label>
                            <input data-testid='maxHInput' name='height.maximum' ref={register({ required: true })} placeholder='Max'  defaultValue={pokemon.height.maximum} />
                        </Form.Field>
                    </Form.Group>
                    <Form.Field>
                        <label>Types</label>
                        <input data-testid='typesInput' name='types' ref={register({ required: true })} placeholder='Types' defaultValue={pokemon.types} />
                    </Form.Field>
                    <Form.Field>
                        <label>Resistant</label>
                        <input data-testid='resistantInput' name='resistant' ref={register({ required: true })} placeholder='Resistant' defaultValue={pokemon.resistant} />
                    </Form.Field>
                    <Form.Field>
                        <label>Flee rate</label>
                        <input data-testid='fleeInput' name='fleeRate' ref={register({ required: true })} placeholder='Flee rate' type="number" defaultValue={parseFloat(pokemon.fleeRate)} />
                    </Form.Field>
                    <Form.Group widths='equal'>
                        <Form.Field>
                            <label>Max HP:</label>
                            <input data-testid='maxHPInput' name='maxHP' ref={register({ required: true })} placeholder='Max HP:' type="number" defaultValue={pokemon.maxHP} />
                        </Form.Field>
                        <Form.Field>
                            <label>Max CP:</label>
                            <input data-testid='maxCPInput' name='maxCP' ref={register({ required: true })} placeholder='Max CP:' type="number" defaultValue={pokemon.maxCP} />
                        </Form.Field>
                    </Form.Group>
                    <Button data-testid='btnSalvar' type='submit' >Salvar</Button>
                </Form>
            </Container>
        )
    }
} 

// const mapStateToProps = (state ) => ({
//   pokemons: state.pokemons,
// })

// const mapDispatchToProps = dispatch => bindActionCreators(actions,dispatch)

// export default connect(mapStateToProps, mapDispatchToProps)(CardPokemon))
export default Edit