import React,{ useEffect } from 'react'
import {  useDispatch, useSelector } from 'react-redux'
import { requestAllPokemons } from '../../store/actions'
import { getAllPokemonsQuery } from '../../store/queries'
import Pokedex from '../Pokedex'
import { getAllPokemons } from '../../store/queries'


import { Container, Loader } from 'semantic-ui-react'


export default function Main() {
    

    const dispatch = useDispatch()
    const pokemons = useSelector(state => state.pokemons.pokemons)
    const loading = useSelector(state => state.pokemons.loading)
    
    
    useEffect(() => {
        console.log('load',loading)
      if (pokemons.length < 1) {
        dispatch(requestAllPokemons())
        getAllPokemons(getAllPokemonsQuery,{first:151}).then(response => {
          dispatch({type: 'SUCCESS_SEARCH_POKEMONS',payload: {data : response.data.pokemons}})
        })
      }
    },[])
  
  
  return (
    <Container fluid>
    {loading && <Loader active >Loading.. </Loader>}    
      <Pokedex />
    </Container>
     

  )
}
