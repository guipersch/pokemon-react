import React,{ useState } from 'react'
import _ from 'lodash'
import { searchPokemon }  from '../../store/actions'
import CardPokemon from '../Card'
import { useSelector, useDispatch } from 'react-redux'

import { CardGroup, Container, Input, Loader } from 'semantic-ui-react'


export default function Pokedex() {
  const pokemons = useSelector(state => state.pokemons.pokemons)
  const loading = useSelector(state => state.pokemons.loading)
  const [search, setSearch] = useState('');
  const dispatch = useDispatch()

const handleSearchChange = (e, { value }) => {
  setSearch(value)
    setTimeout(() => {
      
      dispatch(searchPokemon(value))
    }, 1000)
  }

  const searchStyle = {
    marginLeft:70,
    marginRight:70
  }
  const containerStyle = {
    padding:30
  }
  const groupStyle = {
    marginTop:30
  }
  console.log(pokemons)
  return (
    <Container style={containerStyle} fluid>
     <Input icon='search' placeholder='Search...' 
            fluid
            value={search}
            onChange={handleSearchChange}
            // {_.debounce(handleSearchChange, 500, {
            //   loading: true,
            // })}
            style={searchStyle}
            name='search'
            data-testid='searchInput'
          />
          {console.log('load',loading)}
     
      {!loading && <CardGroup style={groupStyle} centered >
        {pokemons.map(pokemon =>(
          <CardPokemon key={pokemon.number} pokemon={pokemon} />
        ))}
      </CardGroup>}
    </Container>
     

  )
}

// function getAllPokemons(query,variables) {
//   console.log('getAll fetch')
//       const response = fetch(BASE_URL_API, {
//         method: 'POST',
//         headers: { 'Content-Type': 'application/json' },
//         body: JSON.stringify({ query, variables })
//       });
    
//       return response.json();
// }


// class Pokedex extends Component {
//   // eslint-disable-next-line no-useless-constructor
//   // constructor(props) {
//   //   super(props);
//   //   console.log(this.props)

//   // }

//   render() {
//     return (
//       <div>
//         {console.log(this.props)}
//         {
//         this.props.pokemons.map(pokemon =>(
//           <CardPokemon pokemon={pokemon} />
//         ))}
//       </div>
//     );
//   }
// }

// const Pokedex = ({pokemons }) => (
//   <div>
//     {pokemons.map(pokemon =>(
//       <CardPokemon pokemon={pokemon} />
//     ))}
//   </div>
// )

// const mapStateToProps = (state ) => ({
//   pokemons: state.pokemons.data,
// })

// const mapDispatchToProps = dispatch => bindActionCreators(Actions,dispatch)

// export default connect(mapStateToProps, mapDispatchToProps)(Pokedex)