import React from 'react'
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom'
import Main from '../components/Main'
import Detail from '../components/Detail'
import Edit from '../components/Edit'
import history from './history'
import { ConnectedRouter } from 'connected-react-router'
import { useSelector, useDispatch } from 'react-redux'

const Routes = () => {

  return (
    <ConnectedRouter history={history}>
      <Switch>
          <Route exact path="/" component={Main}></Route>
          <Route path="/detalhe" component={Detail}></Route>
          <Route path="/editar" component={Edit}></Route>
          {/* {!pokemon <Redirect to='/'/>} */}
      </Switch>
    </ConnectedRouter>
  )
}

// const mapStateToProps = (state ) => ({
//   pokemons: state.pokemons,
// })

// const mapDispatchToProps = dispatch => bindActionCreators(actions,dispatch)

// export default connect(mapStateToProps, mapDispatchToProps)(CardPokemon))
export default Routes