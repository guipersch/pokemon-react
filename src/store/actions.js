export function requestAllPokemons() {
    return {
        type: 'REQUEST_ALL_POKEMONS'       
    }
}

export function detailPokemon(pokemon) {
    return {
        type: 'DETAIL_POKEMON',
        payload: {
            pokemon
        }       
    }
}

export function savePokemon(pokemon) {
    return {
        type: 'SAVE_POKEMON',
        payload: {
            pokemon
        }       
    }
}

export function searchPokemon(value) {
    return {
        type: 'SEARCH_POKEMON',
        payload: {
            value
        }       
    }
}


