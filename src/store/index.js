import { createStore, combineReducers, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'
import pokemons from './reducers/pokemon'
import root from './sagas'
import { connectRouter, routerMiddleware } from 'connected-react-router'
import history from '../routes/history'


const reducers = combineReducers({
    pokemons: pokemons,
    router: connectRouter(history)
})
const sagaMiddleware = createSagaMiddleware()
const middlewares = [
    sagaMiddleware,
    routerMiddleware(history)
]

// function reducer(state = INITIAL_STATE ,action) {
//     return state
// }

const store = createStore(reducers,
applyMiddleware(...middlewares)
)

sagaMiddleware.run(root)

export default store