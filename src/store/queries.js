const BASE_URL_API = 'https://graphql-pokemon.now.sh/'

export const getAllPokemonsQuery = `query pokemons($first: Int!){
    pokemons(first: $first){
      id
      number
      name
      weight{
        minimum
        maximum
      }
      height{
        minimum
        maximum
      }
      classification
      types
      resistant
      weaknesses
      fleeRate
      maxCP
      maxHP
      image
    }
  }`

 export const getAllPokemons = async (query, variables = {}) => {
    const response = await fetch(BASE_URL_API, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ query, variables })
    });
  
    return response.json();
  }