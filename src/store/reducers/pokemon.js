export const INITIAL_STATE = {
   data: [],
   pokemons:[],
   pokemon:{},
   error: false,
   loading: false
}

export default function pokemons(state = INITIAL_STATE, action) {
    let newArray
    switch(action.type) {
        case 'REQUEST_ALL_POKEMONS':
            return {...state, loading: true}
        case 'SUCCESS_SEARCH_POKEMONS':
            console.log(action.payload.data)
            return {data: action.payload.data, pokemons: action.payload.data, loading: false , error: false}
        case 'FAILURE_SEARCH_POKEMONS':
            return {data: [], loading: false, error: true}
        case 'DETAIL_POKEMON':    
            return {...state, pokemon: action.payload.pokemon, loading: false, error: false}
        case 'SAVE_POKEMON':
            let pokemon = action.payload.pokemon
            newArray = state.data.map( (item) => {
                if (item.id === pokemon.id){
                    item = pokemon
                }
                return item
            })
            return {...state, data: newArray, pokemons: newArray, pokemon: action.payload.pokemon, loading: false, error: false}
        case 'SEARCH_POKEMON':
            newArray = state.data.filter( (item) => {
                //console.log(item)
                if (item.name.toLowerCase().includes(action.payload.value.toLowerCase())){
                    return item
                }
                 
            })
            return {...state, pokemons:newArray, pokemon: action.payload.pokemon, loading: false, error: false}
        default:
            return state
    }
}