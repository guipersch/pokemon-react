import { takeEvery, takeLatest, put, call } from 'redux-saga/effects'
import { getAllPokemonsQuery } from './queries'
import { push } from 'connected-react-router'


const BASE_URL_API = 'https://graphql-pokemon.now.sh/'

function getAllPokemons(query,variables) {
    console.log('getAll fetch')
        const response = fetch(BASE_URL_API, {
          method: 'POST',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify({ query, variables })
        });
      
        return response.json();
}

function* asyncRequestAllPokemons() {
    console.log('request')
    try {
        console.log('try')
        const response = yield call(getAllPokemons,getAllPokemonsQuery,{first:151})
        yield put({ 
            type:'SUCCESS_SEARCH_POKEMON',
            payload: {data:response.data}
        })
    } catch(err) {
        yield put({ 
            type:'FAILURE_SEARCH_POKEMON'
        })
    }
}

function* detailPokemon(action) {
    console.log('entrou aqui no detail')
    try{
        yield put({ 
            type:'DETAIL_POKEMON',
            payload: { pokemon: action.payload.pokemon}
        })
        yield put(push('/detalhe'))
    }catch ( err) {
        console.log(err)
    }
}

export default function* root(){
    console.log('root')
    yield [
        takeEvery('REQUEST_ALL_POKEMONS' , asyncRequestAllPokemons),
        takeEvery('DET_POKEMON' , detailPokemon),
    ]
    console.log('root after')
}